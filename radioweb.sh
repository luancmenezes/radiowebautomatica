#!/bin/sh

DIR1=home/radio
DIR2=/usr/share/doc/ices2/examples/ices-alsa.xml
DIR3=/home/radio/ices-alsa.xml
DIR4=/home/radio/transmissao_radio.sh



TRANSMISSAO_RADIO='
#!/bin/bash
# Radio WEB 

echo " "
echo "Transmissao de radio web em operacao"
echo " "
echo "Para interromper pressione CRTL+C"
echo " "
x=`date +_%F_%H_%M_%S`
mv gravacao_transmissao01.ogg gravacao_transmissao$x.ogg
ices2 ices-alsa.xml
'






echo ''
echo '---- BEM VINDO - INSTALAÇÃO RADIO WEB ----'
echo ''
echo 'Selecione: 1 ou 2'
echo ''
echo '1 - Instalar / Reconfigurar a Radio Web '
echo '2 - Iniciar a Radio Web'
read option

case $option in

    1)
        

    mkdir /home/radio
   yum install ices.x86_64
    cp $DIR2 $DIR1
    echo INSIRA INFORMAÇÕES DA RADIO WEB
    echo ""
    echo 'Escreva o nome da sua Radio..:'
        read nameOfRadio
         nameOfRadio2="$(echo $nameOfRadio | sed 's/ /_/g')"
    echo 'Genero da Radio ..:'
        read nameOfGenre
    echo 'Breve Descrição..:'
        read nameOfDescription
    echo 'Site (digite sem https://)..:'
        read website 
    echo 'Endereço do Servidor..:'
        read hostname
    echo 'Senha Servidor..:'
        read password

        ICESALSA='<?xml version="1.0"?>
<ices>

    <!-- run in background  -->
    <background>0</background>
    <!-- where logs go. -->
    <logpath>/var/log/ices</logpath>
    <logfile>ices.log</logfile>
    <!-- size in kilobytes -->
    <logsize>2048</logsize>
    <!-- 1=error, 2=warn, 3=infoa ,4=debug -->
    <loglevel>4</loglevel>
    <!-- logfile is ignored if this is set to 1 -->
    <consolelog>0</consolelog>
    
    <!-- optional filename to write process id to -->
    <!-- <pidfile>/home/ices/ices.pid</pidfile> -->

    <stream>
        <!-- metadata used for stream listing -->
        <metadata>
            <name>'$nameOfRadio'</name>
            <genre>'$nameOfGenre'</genre>
            <description>'$nameOfDescription'</description>
            <url>http://'$website'</url>
        </metadata>

        <!--    Input module.

            This example uses the "alsa" module. It takes input from the
            ALSA audio device (e.g. line-in), and processes it for live
            encoding.  -->
        <input>
            <module>alsa</module>
            <param name="rate">48000</param>
            <param name="channels">2</param>
            <param name="device">hw:0,0</param>
            <!-- Read metadata (from stdin by default, or -->
            <!-- filename defined below (if the latter, only on SIGUSR1) -->
            <param name="metadata">1</param>
            <param name="metadatafilename">test</param>
        </input>

        <!--    Stream instance.

            You may have one or more instances here.  This allows you to
            send the same input data to one or more servers (or to different
            mountpoints on the same server). Each of them can have different
            parameters. This is primarily useful for a) relaying to multiple
            independent servers, and b) encoding/reencoding to multiple
            bitrates.

            If one instance fails (for example, the associated server goes
            down, etc), the others will continue to function correctly.
            This example defines a single instance doing live encoding at
            low bitrate.  -->

        <instance>
            <!--    Server details.

                You define hostname and port for the server here, along
                with the source password and mountpoint.  -->

            <hostname>'$hostname'</hostname>
            <port>8000</port>
            <password>'$password'</password>
            <mount>/'$nameOfRadio2'.ogg</mount>
            <yp>1</yp>   <!-- allow stream to be advertised on YP, default 0 -->

            <!--    Live encoding/reencoding:

                channels and samplerate currently MUST match the channels
                and samplerate given in the parameters to the alsa input
                module above or the remsaple/downmix section below.  -->

            <encode>  
                <quality>0</quality>
                <samplerate>22050</samplerate>
                <channels>1</channels>
            </encode>

            <!-- stereo->mono downmixing, enabled by setting this to 1 -->
            <downmix>1</downmix>

            <!-- resampling.
            
                Set to the frequency (in Hz) you wish to resample to, -->
             
            <resample>
                <in-rate>44100</in-rate>
                <out-rate>22050</out-rate>
            </resample>

             <savefile>./gravacao_transmissao01.ogg</savefile>
             
        </instance>

    </stream>
</ices>'


    echo "${ICESALSA}" > $DIR3
    echo "${TRANSMISSAO_RADIO}" > $DIR4

    chmod +x /home/radio/transmissao_radio.sh

    cd /home/radio
    ./transmissao_radio.sh
    
        ;;
    2)
        cd /home/radio
        ./transmissao_radio.sh
        ;;
esac
    
